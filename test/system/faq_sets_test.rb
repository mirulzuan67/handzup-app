require "application_system_test_case"

class FaqSetsTest < ApplicationSystemTestCase
  setup do
    @faq_set = faq_sets(:one)
  end

  test "visiting the index" do
    visit faq_sets_url
    assert_selector "h1", text: "Faq Sets"
  end

  test "creating a Faq set" do
    visit faq_sets_url
    click_on "New Faq Set"

    fill_in "Archived", with: @faq_set.archived
    fill_in "Creator", with: @faq_set.creator_id
    fill_in "Description", with: @faq_set.description
    fill_in "Title", with: @faq_set.title
    click_on "Create Faq set"

    assert_text "Faq set was successfully created"
    click_on "Back"
  end

  test "updating a Faq set" do
    visit faq_sets_url
    click_on "Edit", match: :first

    fill_in "Archived", with: @faq_set.archived
    fill_in "Creator", with: @faq_set.creator_id
    fill_in "Description", with: @faq_set.description
    fill_in "Title", with: @faq_set.title
    click_on "Update Faq set"

    assert_text "Faq set was successfully updated"
    click_on "Back"
  end

  test "destroying a Faq set" do
    visit faq_sets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Faq set was successfully destroyed"
  end
end
