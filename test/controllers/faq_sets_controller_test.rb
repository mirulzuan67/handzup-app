require 'test_helper'

class FaqSetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @faq_set = faq_sets(:one)
  end

  test "should get index" do
    get faq_sets_url
    assert_response :success
  end

  test "should get new" do
    get new_faq_set_url
    assert_response :success
  end

  test "should create faq_set" do
    assert_difference('FaqSet.count') do
      post faq_sets_url, params: { faq_set: { archived: @faq_set.archived, creator_id: @faq_set.creator_id, description: @faq_set.description, title: @faq_set.title } }
    end

    assert_redirected_to faq_set_url(FaqSet.last)
  end

  test "should show faq_set" do
    get faq_set_url(@faq_set)
    assert_response :success
  end

  test "should get edit" do
    get edit_faq_set_url(@faq_set)
    assert_response :success
  end

  test "should update faq_set" do
    patch faq_set_url(@faq_set), params: { faq_set: { archived: @faq_set.archived, creator_id: @faq_set.creator_id, description: @faq_set.description, title: @faq_set.title } }
    assert_redirected_to faq_set_url(@faq_set)
  end

  test "should destroy faq_set" do
    assert_difference('FaqSet.count', -1) do
      delete faq_set_url(@faq_set)
    end

    assert_redirected_to faq_sets_url
  end
end
