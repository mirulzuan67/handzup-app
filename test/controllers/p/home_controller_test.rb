require 'test_helper'

class P::HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get p_home_index_url
    assert_response :success
  end

end
