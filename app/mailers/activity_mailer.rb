class ActivityMailer < ApplicationMailer
	def notify_event_status(event, admin)
		@event = event
		@admin = admin

		if @event.pending?
			if @event.assessor
				@subject = "Event Re-submitted | #{@event.title}"
			else
				@subject = "New Event Submitted | #{@event.title}"
			end
			@receiver = admin
		else
			@subject = "Event #{@event.status.humanize} | #{@event.title}"
			@receiver = @event.creator
		end

		mail to: @receiver.email, subject: @subject
	end

	def notify_event_invitation(event, participant)
		@event 				= event
		@participant 	= participant
		mail to: @participant.email, subject: @event.title
	end
end
