class ApplicationMailer < ActionMailer::Base
  default from: 'handzup@cybersecurity.com'
  layout 'mailer'
end
