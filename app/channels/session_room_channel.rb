class SessionRoomChannel < ApplicationCable::Channel
  def subscribed
  	case current_user.class.name
  		when "Participant"
  			sessions = current_user.sessions
  		when "Admin"
  			sessions = Session.all_started
  		else
  			sessions = []
  	end

  	sessions.each do |session|
  		stream_from "SessionRoom:#{session.id}"
  	end
  end

  def unsubscribed
    stop_all_streams
  end
end
