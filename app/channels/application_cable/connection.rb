module ApplicationCable
  class Connection < ActionCable::Connection::Base
  	identified_by :current_user

  	def connect
  		self.current_user = find_verified_user
  		logger.add_tags "#{current_user.class.name}ActCab", current_user.id
  	end

  	protected

  		def find_verified_user
        p_warden = env['warden'].user
        a_warden = env['warden'].user(:admin)

        if p_warden
          current_user = p_warden
        elsif a_warden
          current_user = a_warden
        else
          current_user = nil
        end
          
  			if current_user
          current_user
  			else
  				reject_unauthorized_connection
  			end
  		end
  end
end
