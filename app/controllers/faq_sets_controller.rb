class FaqSetsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_faq_set, only: [
    :show, 
    :edit, 
    :update, 
    :destroy, 
    :settings,
    :create_question,
    :add_question,
    :remove_question,
    :search_question
  ]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped

  def index
    @q        = policy_scope(FaqSet).ransack(params[:q])
    @faq_sets = @q.result.order('created_at DESC')
    authorize @faq_sets

    respond_to do |format|
      format.html {
        @faq_sets = @faq_sets.page(params[:page]).per(10)
      }
      format.json {
        if params[:session_id].present?
          render json: @faq_sets.as_json(session_id: params[:session_id])
        end
      }
    end
  end

  def show
    authorize @faq_set
  end

  def settings
    authorize @faq_set
  end

  def new
    @faq_set = policy_scope(FaqSet).new
    
    if current_admin.huadmin?
      @faq_set = current_admin.created_faq_sets.new
    end

    authorize @faq_set
  end

  def edit
    authorize @faq_set
  end

  def create
    @faq_set = policy_scope(FaqSet).new(faq_set_params)
    
    if current_admin.huadmin?
      @faq_set = current_admin.created_faq_sets.new(faq_set_params)
    end

    authorize @faq_set

    if @faq_set.save
      redirect_to @faq_set, notice: t('msg.created')
    else
      render :new
    end
  end

  def update
    authorize @faq_set

    if @faq_set.update(faq_set_params)
      redirect_to @faq_set, notice: t('msg.updated')
    else
      render :edit
    end
  end

  def destroy
    authorize @faq_set
    @faq_set.destroy
    redirect_to faq_sets_url, notice: t('msg.destroyed')
  end

  def create_question
    @question         = @faq_set.questions.new(question_params)
    @question.creator = current_admin
    @question.origin  = :faq

    authorize @faq_set

    if @question.save
      @faq_set.faq_set_questions.create(question: @question)
      render json: @faq_set.faq_set_questions.find_by(question: @question).as_json
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def add_question
    authorize @faq_set
    @faq_set_question = @faq_set.faq_set_questions.where(question_id: params[:question_id]).first_or_create
    render json: @faq_set_question.as_json
  end

  def remove_question
    authorize @faq_set
    @faq_set_questions = @faq_set.faq_set_questions.where(question_id: params[:question_id]).destroy_all
    head :no_content
  end

  private
    def set_faq_set
      @faq_set = policy_scope(FaqSet).find(params[:id])
    end

    def faq_set_params
      params.require(:faq_set).permit(policy(FaqSet).permitted_attributes)
    end

    def question_params
      params.require(:question).permit(:body, :answer)
    end
end
