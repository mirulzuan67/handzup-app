class P::HomeController < ApplicationController
  def index
  end

  def validate_participant
  	@invitation_code 	= params[:ep][:invitation_code]
  	@email 						= params[:ep][:email]
  	@event 						= Event.all_approved.with_session_started.find_by(invitation_code: @invitation_code)
    @errors           = []

    if @email.blank? || !(@email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i)
      @errors << t("msg.invalid_email")
    end

    if @invitation_code.blank? || @event.nil?
      @errors << t("msg.invalid_code_or_session_is_not_yet_started")
    end

    if @errors.any?
      redirect_to p_root_path, alert: @errors.join(", ")
    else
      @session      = @event.sessions.find_by(status: :started)
      @participant  = Participant.find_by(email: @email)

      if @participant
        @session.session_participants.where(participant_id: @participant.id).first_or_create
        sign_in(@participant, scope: :participant)
        redirect_to p_participant_authenticated_path
      else
        redirect_to new_p_participant_registration_path(invitation_code: @event.invitation_code, email: @email)
      end
    end
  end

  def auth_gate
  	# if session available (check by session_key
  end
end
