class P::RoomsController < ApplicationController
	before_action :authenticate_p_participant!
	before_action :set_session, only: [:show, :create_question]

  def index
  	@participant_sessions = current_p_participant.session_participants.includes(:session)
  end

  def show
  end

  def create_question
  	question = @session.questions.new(question_params)
  	question.origin = :participant
  	question.session_question.participant = current_p_participant
  	question.save
  	SessionQuestionRelayJob.perform_later(question.session_question, "session_question", current_p_participant, question.session_question.status)
  end

  def vote
    act = params[:act]
    sq  = SessionQuestion.find(params[:session_question_id])

    case act
      when "like"
        sq.liked_by current_p_participant
      when "unlike"
        sq.unliked_by current_p_participant
      when "dislike"
        sq.disliked_by current_p_participant
      when "undislike"
        sq.undisliked_by current_p_participant
      else
        return
    end

    SessionQuestionRelayJob.perform_later(sq, "session_vote", current_p_participant, sq.status)
    render json: sq.as_json(participant_id: current_p_participant.id)
  end

  def poll
    act = params[:act]
    poll_item = PollItem.find(params[:poll_item_id])

    case act
      when "vote"
        poll_item.upvote_by current_p_participant
      when "unvote"
        poll_item.unvote_by current_p_participant
      else
        return
    end

    SessionPollRelayJob.perform_later(poll_item, "session_poll") # DONT HAVE TO INCLUDE PT ID
    render json: poll_item.as_json(participant_id: current_p_participant.id) # INCLUDE PT ID
  end

  def submit_survey
    @participant_survey = current_p_participant.participant_surveys.new(participant_survey_params)

    if @participant_survey.save
      render json: @participant_survey.as_json
    else
      render json: @participant_survey.errors, status: :unprocessable_entity
    end
  end

  private
  	def set_session
  		ps = SessionParticipant.find_by(session_key: params[:session_key])
  		@session = ps.session
  	end

  	def question_params
  		params.require(:question).permit(:body)
  	end

    def participant_survey_params
      params.require(:participant_survey).permit(:session_survey_id, participant_survey_responses_attributes: [:question_id, :answer])
    end
end
