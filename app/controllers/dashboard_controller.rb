class DashboardController < ApplicationController
  def index
  	@date = Date.today
  	@years = []
  	@months = []

  	2.times do |i|
  		@years << (@date + i.year).strftime("%Y")
  	end

  	(1..12).each do |i|
  		@months << [Date::MONTHNAMES[i].downcase, i]
  	end


  	if params[:y].present? && !params[:y].blank? && @years.include?(params[:y])
  		@year = params[:y]
  	else
  		@year = @date.strftime("%Y")
  	end

  	if params[:m].present? && !params[:m].blank? && @months.collect{ |m| m[1] }.include?(params[:m].to_i)
  		@month = params[:m]
  		@month_name = Date::MONTHNAMES[params[:m].to_i]
  	else
  		@month = @date.strftime("%m")
  		@month_name = @date.strftime("%B")
  	end

  	@events = if current_admin.em?
  		current_admin.created_events
  	else
  		Event.all
  	end

  	@events = @events.within_month_in_year(@month, @year)
  end

  def search
    @keyword = params[:search][:keyword]

    if @keyword
      @admins     = @keyword.blank? ? [] : Admin.ransack(name_cont: @keyword).result
      @events     = @keyword.blank? ? [] : Event.ransack(title_cont: @keyword).result
      @surveys    = @keyword.blank? ? [] : Survey.ransack(title_cont: @keyword).result
      @faq_sets   = @keyword.blank? ? [] : FaqSet.ransack(title_cont: @keyword).result
      @questions  = @keyword.blank? ? [] : Question.ransack(body_cont: @keyword).result
    end
  end
end
