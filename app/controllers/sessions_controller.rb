class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:add_attachment, :remove_attachment]
  before_action :authenticate_admin!
  before_action :set_event
  before_action :set_session, except: [
    :index, 
    :new, 
    :create
  ]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped

  def index
    @sessions = @event.sessions.order('day ASC, start_time ASC')
    authorize @sessions
  end

  def show
    authorize @session
  end

  def room
    authorize @session
  end

  def pubroom
    authorize @session
  end

  def survey_responses
    authorize @session
    @session_survey = @session.session_surveys.find(params[:ssid])
  end

  def new
    authorize_event
    @session = policy_scope(@event.sessions).new
  end

  def edit
    authorize_event
  end

  def create
    authorize_event
    @session = policy_scope(@event.sessions).new(session_params)

    if @session.save
      redirect_to [@event, @session], notice: t("msg.created")
    else
      render :new
    end
  end

  def update
    authorize_event

    if @session.update(session_params)
      redirect_to [@event, @session], notice: t("msg.updated")
    else
      render :edit
    end
  end

  def update_status
    authorize @session

    @session.update_attribute(:status, params[:status])

    case @session.status
      when "started"
        @event.sessions.all_started.where.not(id: @session.id).each do |session|
          session.update_attribute(:status, :ended)
          SessionStatusRelayJob.perform_later(session)
        end

        if @event.completed?
          @event.update_attribute(:status, :approved)
        end
      when "ended"
        if @event.sessions.all_ended.count == @event.sessions.count
          @event.update_attribute(:status, :completed)
        end
      else
        @event.update_attribute(:status, :approved)
    end

    SessionStatusRelayJob.perform_later(@session)

    redirect_to event_sessions_url, notice: t("msg.updated")
  end

  def update_question_status
    authorize @session
    @session_question = @session.session_questions.where(id: params[:session_question_id]).first
    @prev_qs_status   = @session_question.status
    @session_question.update_attribute(:status, params[:status].to_sym)

    SessionQuestionRelayJob.perform_later(@session_question, "session_question_status", nil, @prev_qs_status)
  end

  # PARTICIPANT

  def import_participants
    require 'csv'

    authorize_event

    @session.update_from_setting = true

    if @session.update(permitted_attributes(@session))
      # IMPORT
      pids = []

      CSV.foreach(@session.participant_list_file.path, headers: true) do |row|
        participant = Participant.where(email: row["Email"]).first_or_create do |u|
          u.email                 = row["Email"]
          u.name                  = row["Name"]
          u.organization          = row["Organization"]
          u.password              = SecureRandom.urlsafe_base64(nil, false)
          u.password_confirmation = u.password
        end
        pids << participant.id
        @session.session_participants.where(participant_id: participant.id).first_or_create
      end

      @session.session_participants.where('participant_id not in (?)', pids).destroy_all

      redirect_to settings_event_session_path(@event, @session), notice: t("msg.imported")
    else
      render :settings
    end
  end

  def destroy
    authorize_event
    @session.destroy
    redirect_to event_sessions_url, notice: t("msg.destroyed")
  end

  def settings
    require 'csv'

    authorize_event
    @attachment = Attachment.new

    respond_to do |format|
      format.html
      format.csv do
        title = (@event.title + @session.title).downcase.gsub(' ', '-').gsub(/[^\w-]/, '')
        headers['Content-Disposition'] = "attachment; filename='participant-list-#{title}.csv'"
      end
    end
  end

  # ATACHMENT

  def add_attachment
    authorize_event
    @session_attachment = @session.session_attachments.where(attachment_id: params[:attachment_id]).first_or_create
    render json: @session_attachment.as_json
  end

  def remove_attachment
    authorize_event
    @session_attachment = @session.session_attachments.where(attachment_id: params[:attachment_id]).destroy_all
    head :no_content
  end

  # FAQ

  def add_faq_set
    authorize_event
    @session_faq = @session.session_faq_sets.where(faq_set_id: params[:faq_set_id]).first_or_create
    render json: @session_faq.as_json
  end

  def remove_faq_set
    authorize_event
    @session_faq = @session.session_faq_sets.where(faq_set_id: params[:faq_set_id]).destroy_all
    head :no_content
  end

  # SURVEY

  def add_survey
    authorize_event
    @session_survey = @session.session_surveys.where(survey_id: params[:survey_id]).first_or_create
    render json: @session_survey.as_json
  end

  def remove_survey
    authorize_event
    @session_survey = @session.session_surveys.where(survey_id: params[:survey_id]).destroy_all
    head :no_content
  end

  def add_poll_item
    authorize @session
    @poll = @session.polls.find(params[:poll_id])
    @poll_item = @poll.poll_items.new(poll_item_params)

    if @poll_item.save
      SessionPollRelayJob.perform_later(@poll_item, "add_poll_item")
      render json: @poll_item.as_json
    else
      render json: @poll_item.errors, status: :unprocessable_entity
    end
  end

  def remove_poll_item
    authorize @session

    @poll_item = PollItem.find(params[:poll_item_id])
    SessionPollRelayJob.perform_later(@poll_item, "remove_poll_item")
    @poll_item.destroy
    head :no_content
  end

  private
    def set_event
      @event = policy_scope(Event, policy_scope_class: EventPolicy::Scope).find(params[:event_id])
    end

    def set_session
      @session = @event.sessions.find(params[:id])
    end

    def session_params
      params.require(:session).permit(policy(Session).permitted_attributes)
    end

    def faq_params
      params.require(:question).permit(:body)
    end

    def authorize_event
      authorize @event, :update?, policy_class: EventPolicy
    end

    def poll_item_params
      params.require(:poll_item).permit(:item)
    end
end
