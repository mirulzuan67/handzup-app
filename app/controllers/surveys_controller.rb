class SurveysController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_survey, only: [
    :show, 
    :edit, 
    :update, 
    :destroy, 
    :settings,
    :create_question,
    :add_question,
    :remove_question,
    :update_question_format
  ]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped

  def index
    @q = policy_scope(Survey).ransack(params[:q])
    @surveys = @q.result.order('created_at DESC')
    authorize @surveys

    respond_to do |format|
      format.html {
        @surveys = @surveys.page(params[:page]).per(10)
      }
      format.json {
        if params[:session_id].present?
          render json: @surveys.as_json(session_id: params[:session_id])
        end
      }
    end
  end

  def show
    authorize @survey
  end

  def settings
    authorize @survey
  end

  def new
    @survey = policy_scope(Survey).new
    
    if current_admin.huadmin?
      @survey = current_admin.created_surveys.new
    end

    authorize @survey
  end

  def edit
    authorize @survey
  end

  def create
    @survey = policy_scope(Survey).new(survey_params)
    
    if current_admin.huadmin?
      @survey = current_admin.created_surveys.new(survey_params)
    end

    authorize @survey

    if @survey.save
      redirect_to @survey, notice: t('msg.created')
    else
      render :new
    end
  end

  def update
    authorize @survey

    if @survey.update(survey_params)
      redirect_to @survey, notice: t('msg.updated')
    else
      render :edit
    end
  end

  def destroy
    authorize @survey
    @survey.destroy
    redirect_to surveys_url, notice: t('msg.destroyed')
  end

  def create_question
    @question         = @survey.questions.new(question_params)
    @question.creator = current_admin
    @question.origin  = :survey

    authorize @survey

    if @question.save
      @survey.survey_questions.create(question: @question)
      render json: @survey.survey_questions.find_by(question: @question).as_json
    else
      render json: @question.errors, status: :unprocessable_entity
    end
  end

  def add_question
    authorize @survey
    @survey_question = @survey.survey_questions.where(question_id: params[:question_id]).first_or_create
    render json: @survey_question.as_json
  end

  def remove_question
    authorize @survey
    @survey_questions = @survey.survey_questions.where(question_id: params[:question_id]).destroy_all
    head :no_content
  end

  def update_question_format
    authorize @survey
    @survey_question = @survey.survey_questions.where(id: params[:survey_question_id]).first
    @survey_question.update_attribute(:question_format, params[:question_format].to_sym)
    render json: @survey_question.as_json
  end

  private
    def set_survey
      @survey = policy_scope(Survey).find(params[:id])
    end

    def survey_params
      params.require(:survey).permit(policy(Survey).permitted_attributes)
    end

    def question_params
      params.require(:question).permit(:body)
    end
end
