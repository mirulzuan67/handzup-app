class QuestionsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped, except: [:new, :create]

  def index
    @q = policy_scope(Question).ransack(params[:q])
    @questions = @q.result.not_from_participants.order('created_at DESC')
    authorize @questions
    
    respond_to do |format|
      format.html {
        @questions = @questions.page(params[:page]).per(10)
      }
      format.json {
        if params[:survey_id].present?
          render json: @questions.as_json(survey_id: params[:survey_id])
        end

        if params[:faq_set_id].present?
          render json: @questions.as_json(faq_set_id: params[:faq_set_id])
        end

        if params[:session_id].present?
          render json: @questions.as_json(session_id: params[:session_id])
        end
      }
    end
  end

  def show
    authorize @question
  end

  def new
    @question = current_admin.created_questions.new
    authorize @question
  end

  def edit
    authorize @question
  end

  def create
    @question = current_admin.created_questions.new(question_params)

    if @question.faq?
      @question.for_faq = true
    else
      @question.for_faq = false
    end

    authorize @question

    if @question.save
      redirect_to @question, notice: t('msg.created')
    else
      render :new
    end
  end

  def update
    authorize @question

    if @question.update(question_params)
      redirect_to @question, notice: t('msg.updated')
    else
      render :edit
    end
  end

  def destroy
    authorize @question
    @question.destroy
    redirect_to questions_url, notice: t('msg.destroyed')
  end

  private
    def set_question
      @question = policy_scope(Question).find(params[:id])
    end

    def question_params
      params.require(:question).permit(policy(Question).permitted_attributes)
    end
end
