class ApplicationController < ActionController::Base
	include Pundit
	protect_from_forgery with: :exception, prepend: true
	before_action :set_locale
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

	private

	def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def default_url_options(options = {})
    {locale: I18n.locale}
  end

  def pundit_user
    if admin_signed_in?
    	current_admin
    end
  end

  def after_sign_out_path_for(resource_or_scope)
    if resource_or_scope == :admin
      admin_unauthenticated_path
    else
      root_path
    end
  end

  def user_not_authorized
    flash[:alert] = "Access denied!"
    redirect_to(request.referrer || admin_authenticated_path)
  end

  def user_for_paper_trail
    admin_signed_in? ? current_admin.id : 'BOT'  # or whatever
  end
end
