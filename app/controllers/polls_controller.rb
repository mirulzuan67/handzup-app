class PollsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_event
  before_action :set_session
  before_action :set_poll, only: [:edit, :update, :destroy]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped

  def new
    authorize_event
    @poll = @session.polls.new
  end

  def edit
    authorize_event
  end

  def create
    authorize_event
    @poll = @session.polls.new(poll_params)

    if @poll.save
      redirect_to settings_event_session_path(@event, @session), notice: t('msg.created')
    else
      render :new
    end
  end

  def update
    authorize_event

    if @poll.update(poll_params)
      redirect_to settings_event_session_path(@event, @session), notice: t('msg.updated')
    else
      render :edit
    end
  end

  def destroy
    authorize_event

    @poll.destroy
    redirect_to settings_event_session_path(@event, @session), notice: t('msg.destroyed')
  end

  private
    def set_event
      @event = policy_scope(Event, policy_scope_class: EventPolicy::Scope).find(params[:event_id])
    end

    def set_session
      @session = @event.sessions.find(params[:session_id])
    end

    def set_poll
      @poll = @session.polls.find(params[:id])
    end

    def poll_params
      params.require(:poll).permit(policy(Poll).permitted_attributes)
    end

    def authorize_event
      authorize @event, :update?, policy_class: EventPolicy
    end
end
