class AdminsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_admin, only: [:show, :edit, :update, :destroy]
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped, only: :index

  def index
    @admins = policy_scope(Admin)
    @admins = @admins.order('created_at DESC').page(params[:page]).per(10)
    authorize @admins
  end

  def show
    authorize @admin
  end

  def new
    @admin = Admin.new
    authorize @admin
  end

  def edit
    authorize @admin
  end

  def create
    @admin      = Admin.new(admin_params)
    @admin.otp  = SecureRandom.hex(4)
    secret      = "handzup2020" # @admin.otp.reverse
    # SET PASS
    @admin.password              = secret
    @admin.password_confirmation = secret
    @admin.skip_confirmation!

    authorize @admin

    if @admin.save
      redirect_to @admin, notice: t("msg.created")
    else
      render :new
    end
  end

  def update
    authorize @admin

    if @admin.update(permitted_attributes(@admin))
      redirect_to @admin, notice: t("msg.updated")
    else
      render :edit
    end
  end

  def destroy
    authorize @admin

    @admin.destroy
    redirect_to admins_url, notice: t("msg.destroyed")
  end

  private
    def set_admin
      @admin = Admin.find(params[:id])
    end

    def admin_params
      params.require(:admin).permit(policy(Admin).permitted_attributes)
    end
end
