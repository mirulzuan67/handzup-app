class EventsController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_event, only: [:show, :edit, :update, :destroy, :review, :approve, :reject, :submit, :cancel, :summary, :send_invitation]
  before_action :set_paper_trail_whodunnit
  include Pundit
  after_action :verify_authorized
  after_action :verify_policy_scoped

  # COMMIT PARAMS
  SUBMIT  = I18n.t('l.submit')
  APPROVE = I18n.t('l.approve')
  REJECT  = I18n.t('l.reject')
  CANCEL  = I18n.t('l.cancel')

  def index
    @events = policy_scope(Event)
    @events = @events.includes(:creator).order('created_at DESC').page(params[:page]).per(10)
    authorize @events
  end

  def show
    @sessions = @event.sessions.includes(
      :attachments,
      :polls,
      :participants,
      session_surveys: [:survey],
      session_faq_sets: [:faq_set]
    ).order('day ASC, start_time ASC')
    
    authorize @event
  end

  def review
    @sessions = @event.sessions.includes(
      :attachments,
      :polls,
      :participants,
      session_surveys: [:survey],
      session_faq_sets: [:faq_set]
    ).order('day ASC, start_time ASC')

    authorize @event
  end

  def summary
    @sessions = @event.sessions.includes(
      :attachments,
      :polls,
      :participants,
      session_surveys: [:survey],
      session_faq_sets: [:faq_set]
    ).order('day ASC, start_time ASC')
    
    authorize @event
  end

  def new
    @event = policy_scope(Event).new
    
    if current_admin.huadmin?
      @event = current_admin.created_events.new
    end

    authorize @event
  end

  def edit
    authorize @event
  end

  def create
    @event = policy_scope(Event).new(event_params)
    
    if current_admin.huadmin?
      @event = current_admin.created_events.new(event_params)
    end

    authorize @event

    if @event.save
      redirect_to event_sessions_path(@event), notice: t('msg.created')
    else
      render :new
    end
  end

  def update
    authorize @event

    if @event.update(event_params)
      redirect_to @event, notice: t('msg.updated')
    else
      render :edit
    end
  end

  def destroy
    authorize @event
    @event.destroy
    redirect_to events_url, notice: t('msg.destroyed')
  end

  def approve
    authorize @event
    @event.status = "approved"
    react_responds
  end

  def reject
    authorize @event
    @event.status = "rejected"
    react_responds
  end

  def submit
    authorize @event
    @event.status = "pending"
    react_responds
  end

  def cancel
    authorize @event
    @event.status = "cancelled"
    react_responds
  end

  def send_invitation
    authorize @event

    @event.sessions.each do |session|
      participants = session.participants

      if participants.any?
        participants.each do |participant|
          InvitationMailerJob.perform_later(@event, participant)
        end
      end
    end
    redirect_to @event, notice: t("msg.invitation_sent")
  end

  private
    def set_event
      @event = policy_scope(Event).find(params[:id])
    end

    def event_params
      params.require(:event).permit(policy(Event).permitted_attributes)
    end

    def react_responds
      if (@event.approved? || @event.rejected?) && @event.assessor.nil?
        @event.assessor = current_admin
      end

      if @event.update(permitted_attributes(@event))
        if @event.pending?
          if @event.assessor
            unless @event.assessor == @event.creator
              ActivityMailerJob.perform_later(@event, @event.assessor)
            end
          else
            unless @event.creator.huadmin?
              Admin.all_huadmin.each do |admin|
                ActivityMailerJob.perform_later(@event, @event.creator)
              end
            end
          end
        elsif @event.approved? || @event.rejected?
          ActivityMailerJob.perform_later(@event, @event.creator)
        else
          # DO NOTHING
        end

        redirect_to review_event_path, notice: t('msg.updated')
      else
        render :review
      end
    end
end
