class SessionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    user.huadmin? || user.em?
  end

  def show?
    if user.huadmin?
      true
    else
      record.event.creator == user
    end
  end

  def room?
    if user.huadmin?
      record.status == 'started'
    else
      record.event.creator == user && record.status == 'started'
    end
  end

  def pubroom?
    room?
  end

  def survey_responses?
    if user.huadmin?
      record.event.status == 'completed'
    else
      record.event.creator == user && record.event.status == 'completed'
    end
  end

  def update_status?
    if user.huadmin?
      record.event.status == 'approved' || record.event.status == 'completed'
    else
      record.event.creator == user && (record.event.status == 'approved' || record.event.status == 'completed')
    end
  end

  def update_question_status?
    if user.huadmin?
      record.status == 'started'
    else
      record.event.creator == user && record.status == 'started'
    end
  end

  def add_poll_item?
    if user.huadmin?
      record.status == 'started'
    else
      record.event.creator == user && record.status == 'started'
    end
  end

  def remove_poll_item?
    add_poll_item?
  end

  def permitted_attributes
    [:start_time, :end_time, :description, :title, :day]
  end

  def permitted_attributes_for_import_participants
    [:participant_list_file]
  end
end
