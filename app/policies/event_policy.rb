class EventPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.huadmin?
      	scope.all
      else
      	scope.where(creator: user)
      end
    end
  end

  def index?
  	user.huadmin? || user.em?
  end

  def show?
  	if user.huadmin?
    	true
    else
    	record.creator == user
    end
  end

  def create?
  	user.huadmin? || user.em?
  end

  def new?
  	create?
  end

  def update?
  	record.creator == user && (record.status == 'draft' || record.status == 'rejected')
  end

  def edit?
  	update?
  end

  def destroy?
  	update?
  end

  def review?
  	if user.em?
    	record.creator == user && record.status != 'completed'
    else
    	record.status != 'completed'
    end
  end

  def summary?
    if user.em?
      record.creator == user && record.status == 'completed'
    else
      record.status == 'completed'
    end
  end

  def approve?
  	record.is_ready_to_be_assessed? && user.huadmin?
  end

  def reject?
  	record.is_ready_to_be_assessed? && user.huadmin?
  end

  def submit?
  	record.is_ready_to_be_submitted? && record.creator == user
  end

  def cancel?
  	update?
  end

  def send_invitation?
    if user.em?
      record.creator == user && record.status == 'approved'
    else
      record.status == 'approved'
    end
  end

  def permitted_attributes
    [:title, :description, :start_date, :end_date, :invitation_type, :invitation_code, attachments_attributes: [:id, :file, :file_cache, :_destroy]]
  end

  def permitted_attributes_for_approve
    [:assessor_remark]
  end

  def permitted_attributes_for_reject
    [:assessor_remark]
  end

  def permitted_attributes_for_submit
    [:creator_remark]
  end

  def permitted_attributes_for_cancel
    [:creator_remark]
  end
end
