class PollPolicy < ApplicationPolicy
  def permitted_attributes
    [:session_id, :title, poll_items_attributes: [:id, :item, :_destroy]]
  end
end
