class QuestionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
  	user.huadmin? || user.em?
  end

  def show?
  	user.huadmin? || user.em?
  end

  def create?
  	user.huadmin? || user.em?
  end

  def new?
  	create?
  end

  def update?
  	if user.huadmin?
    	true
    else
    	record.creator == user
    end
  end

  def edit?
  	update?
  end

  def destroy?
  	update?
  end

  def permitted_attributes
    [:body, :answer, :origin]
  end
end
