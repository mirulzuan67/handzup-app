class SurveyPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
  	user.huadmin? || user.em?
  end

  def show?
  	user.huadmin? || user.em?
  end

  def create?
  	user.huadmin?
  end

  def new?
  	create?
  end

  def update?
  	user.huadmin? && !record.sessions.any?
  end

  def edit?
  	update?
  end

  def settings?
    update?
  end

  def destroy?
  	update?
  end

  def create_question?
    update?
  end

  def remove_question?
    update?
  end

  def update_question_format?
    update?
  end

  def add_question?
    update?
  end

  def permitted_attributes
    [:title, :description]
  end
end
