class AdminPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    user.huadmin?
  end

  def show?
    if user.em?
    	record == user
    else
    	true
    end
  end

  def create?
    user.huadmin?
  end

  def new?
    create?
  end

  def update?
    if user.em?
    	record == user
    else
    	true
    end
  end

  def edit?
    update?
  end

  def destroy?
    user.huadmin?
  end

  def permitted_attributes
    [:email, :name, :staff_id, :contact_number, :image, :role]
  end

  def permitted_attributes_for_update
    [:email, :password, :password_confirmation, :name, :staff_id, :contact_number, :image, :role]
  end
end
