$(document).ready(function () {
  /*------------------------------------------------
      Datetime picker (Flatpickr)
  ------------------------------------------------*/
  // Date and time
  if($('.datetimepicker')[0]) {
    $('.datetimepicker').flatpickr({
      enableTime: true,
      nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
      prevArrow: '<i class="zmdi zmdi-long-arrow-left" />'
    });
  }

  // Date only
  if($('.datepicker')[0]) {
    $('.datepicker').flatpickr({
      enableTime: false,
      nextArrow: '<i class="zmdi zmdi-long-arrow-right" />',
      prevArrow: '<i class="zmdi zmdi-long-arrow-left" />'
    });
  }

  // Time only
  if($('.timepicker')[0]) {
    $('.timepicker').flatpickr({
      noCalendar: true,
      enableTime: true
    });
  }
});