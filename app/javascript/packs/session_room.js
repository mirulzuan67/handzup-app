import Vue from 'vue'
import VueResource from 'vue-resource'
import SessionRoom from '../components/room/SessionRoom.vue'
import axios from 'axios'
import lodash from 'lodash'
import qs from 'qs'
import ActionCable from 'actioncable'

axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.paramsSerializer = (params) => {
	return qs.stringify(params, {
		arrayFormat: 'brackets'
	})
}

let tokenDom = document.querySelector('meta[name=csrf-token]')

if (tokenDom) {
	let csrfToken = tokenDom.content
	axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
}

let cable = ActionCable.createConsumer(process.env.ACTCAB_SOCKET_URL)
Vue.prototype.$cable = cable

document.addEventListener('DOMContentLoaded', () => {
	new Vue({
		el: '#session_room',
		components: {
			SessionRoom
		}
	})
})