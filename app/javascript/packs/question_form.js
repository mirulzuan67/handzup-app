import '../src/plugins/select2/dist/js/select2.full.min'

document.addEventListener('DOMContentLoaded', () => {
	if ($('#question_origin').val() != 'faq') {
		$('#answer_field').hide(); 
	}

  $('#question_origin').select2();
  $('#question_origin').on('select2:select', function (e) { 
  	$('#question_answer').val(null); 

		if ($('#question_origin').val() == 'faq') {
    	$('#answer_field').show(); 
    } else {
    	$('#answer_field').hide(); 
    } 
	});
})