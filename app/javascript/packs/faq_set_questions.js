import Vue from 'vue'
import FaqSetQuestions from '../components/faq/FaqSetQuestions.vue'
import axios from 'axios'
import lodash from 'lodash'
import qs from 'qs'

axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.paramsSerializer = (params) => {
	return qs.stringify(params, {
		arrayFormat: 'brackets'
	})
}

let tokenDom = document.querySelector('meta[name=csrf-token]')

if (tokenDom) {
	let csrfToken = tokenDom.content
	axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
}

document.addEventListener('DOMContentLoaded', () => {
	new Vue({
		el: '#faq_set_questions',
		components: {
			FaqSetQuestions
		}
	})
})