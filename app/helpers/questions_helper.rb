module QuestionsHelper
	def question_origins
		Question.origins.reject{|k|k == "participant"}.map{|k, v|[k.humanize.upcase, k]}
	end
end
