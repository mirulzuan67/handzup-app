module AdminsHelper
	def admin_roles
		Admin.roles.map{|k, v|[k.humanize.upcase, k]}
	end
end
