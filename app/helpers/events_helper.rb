module EventsHelper
	def event_invitation_types
		Event.invitation_types.map{|k, v|[k.humanize.upcase, k]}
	end
end
