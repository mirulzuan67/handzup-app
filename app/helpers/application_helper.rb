module ApplicationHelper
	def page_title(t)
    content_for :title, t.to_s
  end

	def no_result_message
    alert = <<-HTML
      <div class="alert alert-info" role="alert">
        <strong>#{t('msg.no_result')}</strong>
      </div>
    HTML
    alert.html_safe
  end

  def bootstrap_class_for(flash_type)
    hash = HashWithIndifferentAccess.new({ 
      success: "alert-success", 
      error: "alert-danger", 
      alert: "alert-warning", 
      notice: "alert-info" 
    })
    hash[flash_type] || flash_type.to_s
  end

  def flash_messages(opts = {})
    html_all = ""
    flash.each do |msg_type, message|
      html = <<-HTML
      <div class="alert #{bootstrap_class_for(msg_type)}">
        #{message}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        <!-- <span aria-hidden="true">&times;</span> -->
	      </button>
      </div>
      HTML
      html_all += html
    end
    html_all.html_safe
  end

  def nil_checker(v)
    if v
      return v
    else
      return "-"
    end
  end

  def active_class?(controllers, a, is_sub)
    if is_sub
      active_class = "navigation__sub--active navigation__sub--toggled"
    else
      active_class = "navigation__active"
    end

    controllers = controllers.join('|')

    return controller.controller_name =~ /#{controllers}/ ? active_class : ""
  end

  def tooltip_data(label)
    {
      toggle: "tooltip",
      placement: "top",
      original_title: label
    }
    # .merge(tooltip_data(t("l.cancel"))
  end
end
