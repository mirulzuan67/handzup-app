class Admin < ApplicationRecord
  devise 	:database_authenticatable,
  				:recoverable, 
  				:rememberable, 
  				:validatable, 
  				:confirmable, 
  				:lockable, 
  				:trackable, 
  				lock_strategy: :failed_attempts, 
  				unlock_keys: [:email], 
  				unlock_strategy: :email, 
  				maximum_attempts: 20, 
  				reset_password_keys: [:email]
  enum role: [:huadmin, :em]
  has_many :created_events, class_name: 'Event', foreign_key: 'creator_id'
  has_many :assessed_events, class_name: 'Event', foreign_key: 'assessor_id'
  has_many :created_surveys, class_name: 'Survey', foreign_key: 'creator_id'
  has_many :created_faq_sets, class_name: 'FaqSet', foreign_key: 'creator_id'
  has_many :created_questions, class_name: 'Question', foreign_key: 'creator_id'
  validates :name, presence: true, on: :update
  validates :role, presence: true
  scope :all_huadmin, -> { where(role: :huadmin) }
end
