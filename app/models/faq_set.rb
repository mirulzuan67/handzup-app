class FaqSet < ApplicationRecord
	belongs_to :creator, class_name: 'Admin'
	has_many :faq_set_questions
	has_many :questions, through: :faq_set_questions
	has_many :session_faq_sets
	has_many :sessions, through: :session_faq_sets

	def as_json(options = {})
		hash								= {}
		hash["id"]					= id
		hash["title"]				= title
		hash["description"]	= description
		hash["questions"]		= faq_set_questions.includes(question: [:creator]).order('created_at ASC').as_json
		hash["creator"]			= creator.name

		if options[:session_id]
      hash["checked"] = sessions.where(id: options[:session_id]).exists?
    end

		return hash
	end
end
