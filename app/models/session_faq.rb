class SessionFaq < ApplicationRecord
  belongs_to :session
  belongs_to :question

  def as_json(options = {})
		hash 							= {}
		hash["id"]				= id
    
    if options[:session_id]
			hash["question"] = question.as_json(session_id: options[:session_id])
		else
			hash["question"] = question.as_json
		end

    return hash
  end
end
