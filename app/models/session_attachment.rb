class SessionAttachment < ApplicationRecord
  belongs_to :session
  belongs_to :attachment

  def as_json(options = {})
    hash          			= {}
    hash["id"]    			= id
    hash["attachment"] 	= attachment.as_json

    return hash
  end
end
