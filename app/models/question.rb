class Question < ApplicationRecord
	attr_accessor :for_faq
	enum origin: [:faq, :survey, :participant]
	belongs_to :creator, class_name: 'Admin', optional: true
	has_many :survey_questions
	has_many :surveys, through: :survey_questions
	has_many :faq_set_questions
	has_many :faq_sets, through: :faq_set_questions
	has_many :participant_survey_responses
  has_many :participant_surveys, through: :participant_survey_responses
	has_one :session_question
	has_one :participant, through: :session_question
	has_one :session, through: :session_question
	validates :body, presence: true
	validates :answer, presence: true, if: :for_faq
	validates :origin, presence: true
	scope :not_from_participants, -> { where.not(origin: :participant) }

	def as_json(options = {})
		hash						= {}
		hash["id"]			= id
		hash["body"]		= body
		hash["origin"]	= origin.humanize

		if faq?
			hash["answer"] = answer
		end
		
		if creator
			hash["creator"]	= creator.name
		end
		
		if options[:survey_id]
      hash["checked"] = surveys.where(id: options[:survey_id]).exists?
    end

    if options[:faq_set_id]
      hash["checked"] = faq_sets.where(id: options[:faq_set_id]).exists?
    end

    if options[:session_id]
      hash["checked"] = faq_sessions.where(id: options[:session_id]).exists?
    end

    hash["created_at"] = created_at

		return hash
	end
end
