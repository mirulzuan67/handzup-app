class SessionSurvey < ApplicationRecord
  belongs_to :session
  belongs_to :survey
  has_many :participant_surveys
  has_many :participants, through: :participant_surveys

  def as_json(options = {})
		hash 							= {}
		hash["id"]				= id
    
    if options[:session_id]
			hash["survey"] = survey.as_json(session_id: options[:session_id])
		else
			hash["survey"] = survey.as_json
		end

		if options[:participant_id]
			hash["submitted"] = participants.where(id: options[:participant_id]).exists?
		end

    return hash
  end
end
