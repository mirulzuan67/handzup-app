class Event < ApplicationRecord
	has_paper_trail
	enum status: [:draft, :pending, :approved, :rejected, :cancelled, :completed]
	enum invitation_type: [:normal, :qr]
	has_many :sessions, dependent: :destroy
	has_many :attachments, as: :attachable, dependent: :destroy
	belongs_to :creator, 	class_name: 'Admin'
	belongs_to :assessor,	class_name: 'Admin', optional: true
	accepts_nested_attributes_for :attachments, allow_destroy: true
	validates :title, presence: true
	validates :start_date, presence: true, date: { after_or_equal_to: Date.today }, on: :create
	validates :end_date, presence: true, date: { after_or_equal_to: :start_date }
	validates :invitation_code, presence: true
	scope :all_approved, -> { where(status: :approved) }
	scope :all_pending, -> { where(status: :pending) }
	scope :all_rejected, -> { where(status: :rejected) }
	scope :all_completed, -> { where(status: :completed) }
	scope :all_draft, -> { where(status: :draft) }
	scope :with_session_started, -> { includes(:sessions).where(sessions: { status: :started }) }
	scope :within_month_in_year, -> (m, y) { where('extract(month from start_date) = ? AND extract(year from start_date) = ?', m, y) }
	scope :all_today, -> { where(start_date: Date.today) }

	def date
		start_date.strftime("%d-%m-%Y") + " - " + end_date.strftime("%d-%m-%Y")
	end

	def days
		number_of_days 	= (end_date - start_date).to_i + 1
		days 						= []

		(1..(number_of_days)).each do |day|
			date = (start_date + (day - 1)).strftime("%d-%m-%Y")
			object = [date, day]
			days << object
		end

		return days
	end

	def status_color
		case status
			when "pending"
				"info"
			when "approved"
				"success"
			when "rejected"
				"danger"
			when "cancelled"
				"warning"
			when "completed"
				"purple"
			else
				"secondary"
		end
	end

	def is_ready_to_be_submitted?
		sessions.any? && (draft? || rejected?)
	end

	def is_ready_to_be_assessed?
		pending?
	end

	def as_json(options = {})
    hash										= {}
    hash["id"]							= id
    hash["title"]						= title
    hash["start_date"]			= start_date.strftime("%d-%m-%Y")
    hash["end_date"]				= end_date.strftime("%d-%m-%Y")
    hash["description"]			= description
    hash["invitation_type"]	= invitation_type
    hash["invitation_code"]	= invitation_code
    hash["invitation_code"]	= invitation_code

    if options[:session_id]
    	hash["attachments"]	= attachments.as_json(session_id: options[:session_id])
    else
    	hash["attachments"]	= attachments.as_json
    end

    return hash
  end
end
