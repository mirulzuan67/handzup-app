class Session < ApplicationRecord
  attr_accessor :update_from_setting
  mount_uploader :participant_list_file, AttachmentUploader
	enum status: [:not_started, :started, :ended]
  belongs_to :event
  has_many :session_attachments
  has_many :attachments, through: :session_attachments, dependent: :destroy
  has_many :session_surveys, dependent: :destroy
  has_many :surveys, through: :session_surveys
  has_many :session_faq_sets
  has_many :faq_sets, through: :session_faq_sets
  has_many :session_participants, before_add: :set_session_participant_key, dependent: :destroy
  has_many :participants, through: :session_participants
  has_many :session_questions, dependent: :destroy
  has_many :questions, through: :session_questions
  has_many :polls, dependent: :destroy
  has_many :poll_items, through: :polls
  validates :title, presence: true
  validates :start_time, presence: true, uniqueness: { scope: [:day, :event_id] }
  validates :end_time, presence: true, date: { after: :start_time, message: 'must be after start time' }
  validates :day, presence: true
  validates :participant_list_file, presence: true, if: :update_from_setting
  scope :all_started, -> { where(status: :started) }
  scope :all_ended, -> { where(status: :ended) }

  def time
  	start_time.strftime("%I:%M%p") + " - " + end_time.strftime("%I:%M%p")
  end

  def date
    event.days[day - 1][0].to_date
  end

  def status_color
		case status
			when "started"
				"success"
			when "ended"
				"danger"
			else
				"secondary"
		end
	end

	def as_json(options = {})
    hash                      = {}
    hash["id"]                = id
    hash["title"]             = title
    hash["date"]              = date.strftime("%d-%m-%Y")
    hash["start_time"]        = start_time.strftime("%I:%M%p")
    hash["end_time"]          = end_time.strftime("%I:%M%p")
    hash["description"]       = description
    hash["status"]            = status
    hash["event"]             = event.as_json(session_id: id)
    hash["faq_sets"]          = session_faq_sets.includes(faq_set: [:creator]).order('created_at ASC').as_json(session_id: id)
    hash["attachments"]       = session_attachments.as_json

    if options[:participant_id]
      hash["polls"]             = polls.includes(:poll_items).as_json(participant_id: options[:participant_id])
      hash["session_questions"] = session_questions.includes(:participant, question: [:creator]).order('created_at DESC').as_json(participant_id: options[:participant_id])
      hash["surveys"]           = session_surveys.includes(survey: [:creator]).order('created_at ASC').as_json(participant_id: options[:participant_id])
    else
      hash["polls"]             = polls.includes(:poll_items).as_json
      hash["session_questions"] = session_questions.includes(:participant, question: [:creator]).order('created_at DESC').as_json
      hash["surveys"]           = session_surveys.includes(survey: [:creator]).order('created_at ASC').as_json(session_id: id)
    end

    if options[:session_key]
      hash["key"] = options[:session_key]
    end

    return hash
  end

  private

  def set_session_participant_key(sp)
    sp.session_key = SecureRandom.urlsafe_base64(nil, false)
  end
end
