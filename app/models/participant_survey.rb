class ParticipantSurvey < ApplicationRecord
  belongs_to :participant
  belongs_to :session_survey
  has_many :participant_survey_responses, dependent: :destroy
  has_many :questions, through: :participant_survey_responses
  accepts_nested_attributes_for :participant_survey_responses #, allow_destroy: false
end
