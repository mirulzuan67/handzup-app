class ParticipantSurveyResponse < ApplicationRecord
  belongs_to :participant_survey
  belongs_to :question
  validates :answer, presence: true

  LIKER_SCALE_ANSWERS = [I18n.t("l.strongly_disagree"), I18n.t("l.disagree"), I18n.t("l.moderate"), I18n.t("l.agree"), I18n.t("l.strongly_agree")]
end
