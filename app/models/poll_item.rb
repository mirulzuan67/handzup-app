class PollItem < ApplicationRecord
	acts_as_votable
  belongs_to :poll
  validates :item, presence: true

  def as_json(options = {})
		hash 						= {}
		hash["id"] 			= id
		hash["item"] 		= item
		hash["votes"] 	= votes_for.size
		hash["poll_id"] = poll.id

		if options[:participant_id]
      hash["voted"] = Participant.find(options[:participant_id]).voted_for? self
    end

		return hash
	end
end
