class SurveyQuestion < ApplicationRecord
	enum question_format: [:likert_scale, :free_text]
  belongs_to :survey
  belongs_to :question

  def as_json(options = {})
		hash 							= {}
		hash["id"]				= id
		hash["format"] 		= question_format.humanize
    
    if options[:survey_id]
			hash["question"] 	= question.as_json(survey_id: options[:survey_id])
		else
			hash["question"] 	= question.as_json
		end

    return hash
  end
end
