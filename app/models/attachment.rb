class Attachment < ApplicationRecord
	mount_uploader :file, AttachmentUploader
	belongs_to :attachable, polymorphic: true, optional: true
	has_many :session_attachments
	has_many :sessions, through: :session_attachments
	validates :file, presence: true

	def as_json(options = {})
    hash          = {}
    hash["id"]    = id
    hash["name"]  = file.file.filename
    hash["url"]   = file.url

    if options[:session_id]
      hash["checked"] = sessions.where(id: options[:session_id]).exists?
    end

    return hash
  end
end
