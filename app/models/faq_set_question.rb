class FaqSetQuestion < ApplicationRecord
  belongs_to :faq_set
  belongs_to :question

  def as_json(options = {})
		hash 							= {}
		hash["id"]				= id
    
    if options[:faq_set_id]
			hash["question"] 	= question.as_json(faq_set_id: options[:faq_set_id])
		else
			hash["question"] 	= question.as_json
		end

    return hash
  end
end
