class Participant < ApplicationRecord
  acts_as_voter
  devise 	:database_authenticatable, 
  				:registerable, 
  				:recoverable, 
  				:rememberable, 
  				:validatable
  enum role: [:guest, :registered_participant]
  has_many :session_participants
  has_many :sessions, through: :session_participants
  has_many :session_questions
  has_many :questions, through: :session_questions
  has_many :participant_surveys
  has_many :surveys, through: :participant_surveys
  validates :name, presence: true
  validates :organization, presence: true
end
