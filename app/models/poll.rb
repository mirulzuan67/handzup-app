class Poll < ApplicationRecord
  belongs_to :session
  has_many :poll_items, dependent: :destroy
  accepts_nested_attributes_for :poll_items, allow_destroy: true
  validates :title, presence: true

  def as_json(options = {})
		hash 					= {}
		hash["id"] 		= id
		hash["title"] = title

		if options[:participant_id]
			hash["poll_items"] 	= poll_items.as_json(participant_id: options[:participant_id])
		else
			hash["poll_items"] 	= poll_items.as_json
		end

		return hash
	end
end
