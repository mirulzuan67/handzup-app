class SessionParticipant < ApplicationRecord
  belongs_to :session
  belongs_to :participant
end
