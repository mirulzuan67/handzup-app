class SessionFaqSet < ApplicationRecord
  belongs_to :session
  belongs_to :faq_set

  def as_json(options = {})
		hash 							= {}
		hash["id"]				= id
    
    if options[:session_id]
			hash["faq_set"] = faq_set.as_json(session_id: options[:session_id])
		else
			hash["faq_set"] = faq_set.as_json
		end

    return hash
  end
end
