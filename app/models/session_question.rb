class SessionQuestion < ApplicationRecord
	acts_as_votable
	enum status: [:accepted, :banned, :answered]
  belongs_to :session
  belongs_to :question
  belongs_to :participant

  def likers
  	get_likes.size - get_dislikes.size
  end

  def as_json(options = {})
		hash								= {}
		hash["id"] 					= id
		hash["status"]			= status
		hash["likers"]			= likers
		hash["participant"] = participant.as_json(only: [:id, :name])
		hash["question"] 		= question.as_json

		if options[:participant_id]
      hash["liked"] 		= Participant.find(options[:participant_id]).liked? self
      hash["disliked"] 	= Participant.find(options[:participant_id]).disliked? self
    end

		return hash
	end
end
