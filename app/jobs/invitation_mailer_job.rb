class InvitationMailerJob < ApplicationJob
  queue_as :default

  def perform(event, participant)
    ActivityMailer.notify_event_invitation(event, participant).deliver_later
  end
end
