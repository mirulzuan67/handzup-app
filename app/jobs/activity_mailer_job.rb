class ActivityMailerJob < ApplicationJob
  queue_as :default

  def perform(event, admin)
    ActivityMailer.notify_event_status(event, admin).deliver_later
  end
end
