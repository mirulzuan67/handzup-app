class SessionQuestionRelayJob < ApplicationJob
  queue_as :default

  def perform(session_question, scope, user, prev_status)
    ActionCable.server.broadcast "SessionRoom:#{session_question.session.id}", {
    	session_question: user && user.class.name == "Participant" ? session_question.as_json(participant_id: user.id) : session_question,
    	scope: scope,
    	prev_status: prev_status ? prev_status : "pending",
    	room_id: session_question.session.id
    }
  end
end
