class SessionStatusRelayJob < ApplicationJob
  queue_as :default

  def perform(session)
    ActionCable.server.broadcast "SessionRoom:#{session.id}", {
    	status: session.status,
    	scope: "session_status",
    	room_id: session.id
    }
  end
end
