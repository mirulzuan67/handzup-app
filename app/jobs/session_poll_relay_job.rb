class SessionPollRelayJob < ApplicationJob
  queue_as :default

  def perform(poll_item, scope)
  	session = poll_item.poll.session

    ActionCable.server.broadcast "SessionRoom:#{session.id}", {
    	poll_item: poll_item,
    	scope: scope,
    	room_id: session.id
    }
  end
end
