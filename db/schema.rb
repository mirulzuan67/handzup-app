# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_03_084158) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.string "name"
    t.string "staff_id"
    t.string "contact_number"
    t.string "image"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "otp"
    t.boolean "active"
    t.index ["confirmation_token"], name: "index_admins_on_confirmation_token", unique: true
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_admins_on_unlock_token", unique: true
  end

  create_table "attachments", force: :cascade do |t|
    t.string "file"
    t.string "attachable_type"
    t.bigint "attachable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachable_id", "attachable_type"], name: "index_attachments_on_attachable_id_and_attachable_type"
    t.index ["attachable_type", "attachable_id"], name: "index_attachments_on_attachable_type_and_attachable_id"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer "priority", default: 0, null: false
    t.integer "attempts", default: 0, null: false
    t.text "handler", null: false
    t.text "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string "locked_by"
    t.string "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.date "start_date"
    t.integer "creator_id"
    t.integer "assessor_id"
    t.text "description"
    t.text "creator_remark"
    t.text "assessor_remark"
    t.integer "status", default: 0
    t.integer "invitation_type", default: 0
    t.string "invitation_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "end_date"
    t.integer "invitation_counter", default: 0
    t.index ["assessor_id"], name: "index_events_on_assessor_id"
    t.index ["creator_id"], name: "index_events_on_creator_id"
    t.index ["invitation_code"], name: "index_events_on_invitation_code", unique: true
  end

  create_table "faq_set_questions", force: :cascade do |t|
    t.bigint "faq_set_id"
    t.bigint "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["faq_set_id"], name: "index_faq_set_questions_on_faq_set_id"
    t.index ["question_id"], name: "index_faq_set_questions_on_question_id"
  end

  create_table "faq_sets", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "creator_id"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_faq_sets_on_creator_id"
  end

  create_table "participant_survey_responses", force: :cascade do |t|
    t.bigint "participant_survey_id"
    t.bigint "question_id"
    t.text "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participant_survey_id"], name: "index_participant_survey_responses_on_participant_survey_id"
    t.index ["question_id"], name: "index_participant_survey_responses_on_question_id"
  end

  create_table "participant_surveys", force: :cascade do |t|
    t.bigint "participant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "session_survey_id"
    t.index ["participant_id"], name: "index_participant_surveys_on_participant_id"
    t.index ["session_survey_id"], name: "index_participant_surveys_on_session_survey_id"
  end

  create_table "participants", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "name"
    t.string "organization"
    t.string "contact_number"
    t.integer "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_participants_on_email", unique: true
    t.index ["reset_password_token"], name: "index_participants_on_reset_password_token", unique: true
  end

  create_table "poll_items", force: :cascade do |t|
    t.bigint "poll_id"
    t.string "item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["poll_id"], name: "index_poll_items_on_poll_id"
  end

  create_table "polls", force: :cascade do |t|
    t.bigint "session_id"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_polls_on_session_id"
  end

  create_table "questions", force: :cascade do |t|
    t.text "body"
    t.integer "origin"
    t.integer "creator_id"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "answer"
    t.index ["creator_id"], name: "index_questions_on_creator_id"
  end

  create_table "session_attachments", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "attachment_id"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachment_id"], name: "index_session_attachments_on_attachment_id"
    t.index ["session_id"], name: "index_session_attachments_on_session_id"
  end

  create_table "session_faq_sets", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "faq_set_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["faq_set_id"], name: "index_session_faq_sets_on_faq_set_id"
    t.index ["session_id"], name: "index_session_faq_sets_on_session_id"
  end

  create_table "session_faqs", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "question_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_session_faqs_on_question_id"
    t.index ["session_id"], name: "index_session_faqs_on_session_id"
  end

  create_table "session_participants", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "participant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "session_key"
    t.index ["participant_id"], name: "index_session_participants_on_participant_id"
    t.index ["session_id"], name: "index_session_participants_on_session_id"
    t.index ["session_key"], name: "index_session_participants_on_session_key", unique: true
  end

  create_table "session_questions", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "question_id"
    t.bigint "participant_id"
    t.integer "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["participant_id"], name: "index_session_questions_on_participant_id"
    t.index ["question_id"], name: "index_session_questions_on_question_id"
    t.index ["session_id"], name: "index_session_questions_on_session_id"
  end

  create_table "session_surveys", force: :cascade do |t|
    t.bigint "session_id"
    t.bigint "survey_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_session_surveys_on_session_id"
    t.index ["survey_id"], name: "index_session_surveys_on_survey_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "title"
    t.time "start_time"
    t.time "end_time"
    t.bigint "event_id"
    t.integer "status", default: 0
    t.text "description"
    t.text "remark"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "participant_list_file"
    t.integer "day"
    t.index ["event_id"], name: "index_sessions_on_event_id"
    t.index ["start_time", "day", "event_id"], name: "index_sessions_on_start_time_and_day_and_event_id", unique: true
  end

  create_table "survey_questions", force: :cascade do |t|
    t.bigint "survey_id"
    t.bigint "question_id"
    t.integer "question_format", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["question_id"], name: "index_survey_questions_on_question_id"
    t.index ["survey_id"], name: "index_survey_questions_on_survey_id"
  end

  create_table "surveys", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "creator_id"
    t.boolean "archived", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["creator_id"], name: "index_surveys_on_creator_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  create_table "votes", force: :cascade do |t|
    t.string "votable_type"
    t.bigint "votable_id"
    t.string "voter_type"
    t.bigint "voter_id"
    t.boolean "vote_flag"
    t.string "vote_scope"
    t.integer "vote_weight"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope"
    t.index ["votable_type", "votable_id"], name: "index_votes_on_votable_type_and_votable_id"
    t.index ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope"
    t.index ["voter_type", "voter_id"], name: "index_votes_on_voter_type_and_voter_id"
  end

  add_foreign_key "faq_set_questions", "faq_sets"
  add_foreign_key "faq_set_questions", "questions"
  add_foreign_key "participant_survey_responses", "participant_surveys"
  add_foreign_key "participant_survey_responses", "questions"
  add_foreign_key "participant_surveys", "participants"
  add_foreign_key "participant_surveys", "session_surveys"
  add_foreign_key "poll_items", "polls"
  add_foreign_key "polls", "sessions"
  add_foreign_key "session_attachments", "attachments"
  add_foreign_key "session_attachments", "sessions"
  add_foreign_key "session_faq_sets", "faq_sets"
  add_foreign_key "session_faq_sets", "sessions"
  add_foreign_key "session_faqs", "questions"
  add_foreign_key "session_faqs", "sessions"
  add_foreign_key "session_participants", "participants"
  add_foreign_key "session_participants", "sessions"
  add_foreign_key "session_questions", "participants"
  add_foreign_key "session_questions", "questions"
  add_foreign_key "session_questions", "sessions"
  add_foreign_key "session_surveys", "sessions"
  add_foreign_key "session_surveys", "surveys"
  add_foreign_key "sessions", "events"
  add_foreign_key "survey_questions", "questions"
  add_foreign_key "survey_questions", "surveys"
end
