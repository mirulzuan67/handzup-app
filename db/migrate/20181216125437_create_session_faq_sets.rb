class CreateSessionFaqSets < ActiveRecord::Migration[5.2]
  def change
    create_table :session_faq_sets do |t|
      t.references :session, foreign_key: true
      t.references :faq_set, foreign_key: true

      t.timestamps
    end
  end
end
