class ChangeDescDataTypeInSurveys < ActiveRecord::Migration[5.2]
  def up
  	change_column :surveys, :description, :text
  end

  def down
  	change_column :surveys, :description, :string
  end
end
