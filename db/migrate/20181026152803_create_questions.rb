class CreateQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :questions do |t|
      t.text :body
      t.integer :origin
      t.integer :creator_id, foreign_key: true
      t.boolean :archived, default: false

      t.timestamps
    end
    add_index :questions, :creator_id
  end
end
