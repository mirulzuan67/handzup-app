class AddSessionSurveyToParticipantSurveys < ActiveRecord::Migration[5.2]
  def change
    add_reference :participant_surveys, :session_survey, foreign_key: true
  end
end
