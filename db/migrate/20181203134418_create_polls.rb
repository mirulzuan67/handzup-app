class CreatePolls < ActiveRecord::Migration[5.2]
  def change
    create_table :polls do |t|
      t.references :session, foreign_key: true
      t.string :title

      t.timestamps
    end
  end
end
