class CreateFaqSets < ActiveRecord::Migration[5.2]
  def change
    create_table :faq_sets do |t|
      t.string :title
      t.text :description
      t.integer :creator_id, foreign_key: true
      t.boolean :archived, default: false

      t.timestamps
    end
    add_index :faq_sets, :creator_id
  end
end
