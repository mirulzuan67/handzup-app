class RemoveSurveyFromParticipantSurveys < ActiveRecord::Migration[5.2]
  def change
    remove_reference :participant_surveys, :survey, foreign_key: true
  end
end
