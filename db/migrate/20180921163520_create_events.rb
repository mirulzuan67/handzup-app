class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.date :date
      t.integer :creator_id, foreign_key: true
      t.integer :assessor_id, foreign_key: true
      t.text :description
      t.text :creator_remark
      t.text :assessor_remark
      t.integer :status, default: 0
      t.integer :invitation_type, default: 0
      t.string :invitation_code

      t.timestamps
    end
    add_index :events, :creator_id
    add_index :events, :assessor_id
    add_index :events, :invitation_code, unique: true
  end
end
