class RenameDateToStartDateFromEvents < ActiveRecord::Migration[5.2]
  def up
  	rename_column :events, :date, :start_date
  end

  def down
  	rename_column :events, :start_date, :date
  end
end
