class AddParticipantListFileToSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :participant_list_file, :string
  end
end
