class CreateSessions < ActiveRecord::Migration[5.2]
  def change
    create_table :sessions do |t|
      t.string :title
      t.time :start_time
      t.time :end_time
      t.references :event, foreign_key: true
      t.integer :status, default: 0
      t.text :description
      t.text :remark

      t.timestamps
    end
  end
end
