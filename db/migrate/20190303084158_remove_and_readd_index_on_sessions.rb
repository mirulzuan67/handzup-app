class RemoveAndReaddIndexOnSessions < ActiveRecord::Migration[5.2]
  def change
  	remove_index :sessions, [:start_time, :day]
  	add_index :sessions, [:start_time, :day, :event_id], unique: true
  end
end
