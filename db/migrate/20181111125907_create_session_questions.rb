class CreateSessionQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :session_questions do |t|
      t.references :session, foreign_key: true
      t.references :question, foreign_key: true
      t.references :participant, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
