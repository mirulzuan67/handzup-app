class AddInvitationCounterToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :invitation_counter, :integer, default: 0
  end
end
