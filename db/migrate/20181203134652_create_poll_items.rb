class CreatePollItems < ActiveRecord::Migration[5.2]
  def change
    create_table :poll_items do |t|
      t.references :poll, foreign_key: true
      t.string :item

      t.timestamps
    end
  end
end
