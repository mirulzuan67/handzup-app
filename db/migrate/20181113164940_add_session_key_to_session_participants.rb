class AddSessionKeyToSessionParticipants < ActiveRecord::Migration[5.2]
  def change
    add_column :session_participants, :session_key, :string
    add_index :session_participants, :session_key, unique: true
  end
end
