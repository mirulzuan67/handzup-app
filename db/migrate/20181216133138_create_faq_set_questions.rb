class CreateFaqSetQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :faq_set_questions do |t|
      t.references :faq_set, foreign_key: true
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
