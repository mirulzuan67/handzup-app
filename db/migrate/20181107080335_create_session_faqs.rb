class CreateSessionFaqs < ActiveRecord::Migration[5.2]
  def change
    create_table :session_faqs do |t|
      t.references :session, foreign_key: true
      t.references :question, foreign_key: true

      t.timestamps
    end
  end
end
