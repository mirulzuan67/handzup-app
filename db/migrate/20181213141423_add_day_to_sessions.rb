class AddDayToSessions < ActiveRecord::Migration[5.2]
  def change
    add_column :sessions, :day, :integer
    add_index :sessions, [:start_time, :day], unique: true
  end
end
