class CreateSurveyQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :survey_questions do |t|
      t.references :survey, foreign_key: true
      t.references :question, foreign_key: true
      t.integer :question_format, default: 0

      t.timestamps
    end
  end
end
