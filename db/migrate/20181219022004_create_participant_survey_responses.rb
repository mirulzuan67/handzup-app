class CreateParticipantSurveyResponses < ActiveRecord::Migration[5.2]
  def change
    create_table :participant_survey_responses do |t|
      t.references :participant_survey, foreign_key: true
      t.references :question, foreign_key: true
      t.text :answer

      t.timestamps
    end
  end
end
