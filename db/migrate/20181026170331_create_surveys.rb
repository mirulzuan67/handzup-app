class CreateSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :surveys do |t|
      t.string :title
      t.string :description
      t.integer :creator_id, foreign_key: true
      t.boolean :archived, default: false

      t.timestamps
    end
    add_index :surveys, :creator_id
  end
end
