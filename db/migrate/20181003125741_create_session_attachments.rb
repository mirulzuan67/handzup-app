class CreateSessionAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :session_attachments do |t|
      t.references :session, foreign_key: true
      t.references :attachment, foreign_key: true
      t.boolean :archived, default: false

      t.timestamps
    end
  end
end
