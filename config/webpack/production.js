process.env.NODE_ENV = process.env.NODE_ENV || 'production'

const environment = require('./environment')

const webpack = require('webpack');

environment.config.resolve.alias = {
	'vue$': 'vue/dist/vue.esm.js'
}

environment.plugins.append('Define', new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify('production'),
  'process.env.ACTCAB_SOCKET_URL': JSON.stringify('ws:www.handzup.tech:80/cable'),
}));

module.exports = environment.toWebpackConfig()
