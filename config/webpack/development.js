process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const environment = require('./environment')

const webpack = require('webpack');

environment.config.resolve.alias = {
	'vue$': 'vue/dist/vue.esm.js'
}

environment.plugins.append('Define', new webpack.DefinePlugin({
  'process.env.NODE_ENV': JSON.stringify('development'),
  'process.env.ACTCAB_SOCKET_URL': JSON.stringify('ws:handzup.lvh.me:3000/cable'),
}));

module.exports = environment.toWebpackConfig()
