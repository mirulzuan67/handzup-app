# README

This README would normally document whatever steps are necessary to get the
application up and running in PRODUCTION.

# Things you may want to make sure installed in your PRODUCTION server:
* Ruby v2.5.1 - install with rvm
* NodeJs v12.12.0
* Yarn v1.19.1
* Redis v5.0.7
* PostgreSQL v11.5

# Init config in PRODUCTION server

### 1 - CREATE APP CONFIG PATH
``` bash
$ mkdir hu && mkdir hu/shared && mkdir hu/shared/config
```

### 2 - GO TO CONFIG DIR
``` bash
$ cd hu/shared/config
```

### 3 - CREATE ENV VARIABLES FILE
* create file application.yml and paste below snippets and save

```

production:
  SECRET_KEY_BASE: fbb47b11403d567e8b05ec325237481b78c619cc5d76c223e45ae7c0a309f4405402ccbc183ec5e22b6b71ac97e34f43a413daffd928d1066e351b0cbd3199e0
  DB_U: your_db_username
  DB_P: your_db_password
  SMTP_USER: your_smtp_username
  SMTP_PASS: your_smtp_password
  CMS_URL: http://handzup.domain.com
  PUB_URL: http://handzup.domain.com
  CMS_DOMAIN: handzup.domain.com
  PUB_DOMAIN: handzup.domain.com

```

### 4 - CREATE DATABASE FILE
* create file database.yml and paste below snippets and save

```

default: &default
  adapter: postgresql
  encoding: utf8
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  host: localhost

production:
  <<: *default
  database: hu_production
  username: <%= ENV['DB_U'] %>
  password: <%= ENV['DB_P'] %>

```

### 5 - CREATE SECRETS FILE
* create file secrets.yml and paste below snippets and save

```

production:
  secret_key_base: <%= ENV["SECRET_KEY_BASE"] %>

```