set :default_env, { 
	'NODE_ENV' => 'production' 
}
set :stage, :staging
set :branch, :staging
set :deploy_to, "/home/dev/hu"
server '172.104.189.227', user: 'dev', roles: %w{web app db}