set :default_env, { 
	'NODE_ENV' => 'production' 
}
set :stage, :production
set :branch, :master
set :deploy_to, "/home/tangan/app"
server '34.64.180.132', user: 'tangan', roles: %w{web app db}