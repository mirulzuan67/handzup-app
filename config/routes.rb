Rails.application.routes.draw do
	constraints subdomain: /^(www)$/ do
		scope '(:locale)', locale: /ms|en/ do
			mount ActionCable.server => 'cable'
			root 'home#index'
			namespace :p do
				# PARTICIPANT
				devise_for :participants, path: 'participant_auth', controllers: { 
					registrations: 'participant_auth/registrations',
					sessions: 'participant_auth/sessions'
				}
				authenticated :participant do 
					devise_scope :participant do 
						root to: 'rooms#index', as: 'participant_authenticated' 
					end
				end
				unauthenticated do
					devise_scope :participant do
						root to: 'home#index', as: 'participant_unauthenticated'
					end
				end
				post '/validate', to: 'home#validate_participant', as: 'validate_participant'
				resources :rooms, only: [:index, :show], param: :session_key do
					member do
						post :create_question
						patch :vote
						patch :poll
						post :submit_survey
					end
				end
				root 'rooms#index'
				# PARTICIPANT
			end
			# ADMIN
			scope :admin do
				devise_for :admins, path: 'admins', controllers: { 
					sessions: 'admins/sessions', 
					passwords: 'admins/passwords',
					confirmations: 'admins/confirmations',
					unlocks: 'admins/unlocks'
				}, path_names: {
					sign_in: 'login',
					sign_out: 'logout',
					password: 'secret',
					confirmation: 'verification',
					unlock: 'unblock',
					sign_up: 'logup'
				}
				authenticated :admin do 
					devise_scope :admin do 
						root to: 'dashboard#index', as: 'admin_authenticated' 
					end
				end
				unauthenticated do
					devise_scope :admin do
						root to: 'admins/sessions#new', as: 'admin_unauthenticated'
					end
				end
				resources :admins
				resources :surveys do
					member do
						get :settings
						get :search_question
						post :add_question
						post :create_question
						post :update_question_format
						delete :remove_question
					end
				end
				resources :faq_sets do
					member do
						get :settings
						get :search_question
						post :add_question
						post :create_question
						post :update_question_format
						delete :remove_question
					end
				end
  			resources :questions
				resources :events do
					member do
						get :review
						get :summary
						patch :react, constraints: CommitParamRouting.new(EventsController::SUBMIT), action: :submit
						patch :react, constraints: CommitParamRouting.new(EventsController::APPROVE), action: :approve
						patch :react, constraints: CommitParamRouting.new(EventsController::REJECT), action: :reject
						patch :react, constraints: CommitParamRouting.new(EventsController::CANCEL), action: :cancel
						post :send_invitation
					end
					resources :sessions do
						member do
							get :room
							get :pubroom
							get :settings
							post :add_attachment
							post :add_faq_set
							post :add_survey
							post :update_status
							post :add_poll_item
							post :update_question_status
							patch :import_participants
							delete :remove_attachment
							delete :remove_faq_set
							delete :remove_survey
							delete :remove_poll_item
							get :survey_responses
						end
						resources :polls, path: '/settings/polls', except: [:index, :show]
					end
				end

				#SEARCH
				match 'search' => 'dashboard#search', via: [:get, :post], as: 'search'
			end
			# ADMIN
		end
	end
end
