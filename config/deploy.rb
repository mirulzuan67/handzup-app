# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :rvm_ruby_version, '2.5.1@hu'
set :stages, ['staging', 'production']
set :default_stage, 'staging'
set :application, 'handzup'
set :repo_url, 'git@bitbucket.org:mirulzuan67/handzup-app.git'
set :deploy_via, :remote_cache
set :ssh_options, {
	:forward_agent => true
}
set :linked_files, fetch(:linked_files, []).push(
	'config/database.yml',
	'config/secrets.yml', 
	'config/application.yml')
set :linked_dirs, fetch(:linked_dirs, []).push(
	'log',
	'tmp/pids', 
	'tmp/cache', 
	'tmp/sockets', 
	'public/uploads',
	'public/packs',
	'node_modules')

Rake::Task["deploy:assets:backup_manifest"].clear_actions

after 'deploy:publishing', 'deploy:restart'

namespace :deploy do
  task :restart do
    # invoke 'delayed_job:restart'
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end
end