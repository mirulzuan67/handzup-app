# README

This README would normally document whatever steps are necessary to get the
application up and running using DOCKER. Hence, do install Docker before proceed.

Things you may want to cover:

# Init

``` bash

# BUILD
$ docker-compose build

# SETUP DB
$ docker-compose run --rm web rails db:setup

# RUN APP
$ docker-compose up

```

# Update NODE Deps

* In another terminal tab........

``` bash

# OPEN CONTAINER BASH
$ docker exec -it handzup-app_web_1 /bin/bash

# REBUILD NODE-SASS
$ npm rebuild node-sass

# EXIT
$ exit

```

# Deployment ([READ THIS FIRST!!!](https://bitbucket.org/mirulzuan67/handzup-app/src/master/config/deploy/README.md))

``` bash

# UPDATE SOURCES
$ git pull

# DEPLOY
$ cap production deploy

```